# UnionCoin

A capitalism hard counter.

## Motivation

This system is built as a means to balance power between people, companies, and governments.

Large companies are heavily advantaged in availability of capital and thus in availability of time. In the mid to late twentieth century this was countered by unions on a per company basis. For example, a manufacturing plant in a city can handle a single employee quitting over grievances, but cannot sustain a 50% loss in its workforce.

In the twenty-first century organizations instead have thousands of operational facilities in many different cities, making it difficult or impossible for employees to effectively organize.

Further, the intense advantage held by companies allows them to prevent unionization before it gains any traction. There is little to no enforcement of laws designed to protect the right to unionize, leaving companies to produce and disperse propaganda, as well as terminate employees, or groups of employees, espousing union ideologies.

## Need for Change

_Note_: This section primarily pertains to the United States.

Although companies have continued to grow in size unions have seen a steep decrease in membership since their peak in the mid-twentieth century.

<img src="https://i.imgur.com/NTtCsz4.png" />

Union membership in the US is at it's lowest point since 1932; just 7%.

### This leads to the question: why?

Workers still exist, companies still have employees, and there are very publicly documented cases of poor working conditions and companies taking advantage of lax labor laws.

The obvious answer is that companies developed mechanisms to reduce union formation and participation. This can be seen as anti-union propaganda and in more aggressive cases events like employees being fired for pro-union sentiment.

The United States has a long, sordid history of resisting workers rights. Companies have the right to construct agreements with each other, to internally make decisions about employees; collectively the owners of the company have agency over the workers existence. Why should workers not have similarly powerful agency over the companies existence?

Why should worker agency be regulated by anything other than people's willingness to participate? Instead of artificially limiting collective action why not try a laissez-faire approach, allowing collective human instinct to develop it's own conception of right and wrong in the actions that are taken?

What if like physical training this collective action strengthens the companies ability to handle competition? The workers and the company have a common interest in the company existing; they simply disagree about what is necessary for the company to exist. Both have a common goal, supplying a fair playing field is the job of regulatory entities. Empowering workers will radiate happiness into their lives by virtue of improving their existence. This radiation will necessarily propagate outward as humans interact with each other. Trickle down economics might not work, but trickle up happiness likely will.

### Example Case

Assume a company operates 5000 local branches in various locations. Employees of each branch have no contact with other branches.

Each branch brings in a net revenue of $200,000 per month.
Each branch receives daily shipments of perishable goods totaling $10,000.
Each branch employs 200 individuals at a rate of $2000 per month, per individual.

In the case of employees threatening to organize and requesting a bump to $4000/employee/month the cost to operate the branch is effectively doubled.

The company can either give in to the demands and sustain a loss of $400,000 per month; or it can shut down the branch for two months and rehire entirely new employees at a later point in time; exploiting the employees inability to financially sustain themselves in the short term.

When this decision is made in a vacuum (e.g. without considering human rights/needs) the obvious decision is close the branch, stop shipments, and reopen at a future point in time.

_Without protection for employees companies will always optimize for revenue_. In developed nations with capitalism implementations this is countered by government regulation. When the government fails to fulfill this obligation society level issues manifest as increased poverty, lower life expectancy, lower human development ([IHDI](https://en.wikipedia.org/wiki/List_of_countries_by_inequality-adjusted_HDI)), and increased income disparity.

In the above example UnionCoin is presumed not to exist. If you add in the existence of UnionCoin and suppose that 25% of the above company employees are members of UnionCoin an effective strike in a decentralized fashion could prove effective.

A 10% loss of monthly revenue at 25% of the branches would yield a loss of $25 million per month. Additionally it would initially create more work for higher up employees as it is not clear from the outset which employees will or will not be available for work.

Assuming this type of strike is sustained the company will have to either systemically fire and rehire employees at 25% of it's branches, or negotiate with the union.

## The Union

UnionCoin is a decentralized, anonymous, multi-company union. As companies grow so too should unions. There are many advantages to unionizing anonymously (to the company) and across many companies:

- Ability to move between companies without switching unions
- Prevention of termination for disseminating union information (they can't fire anonymous)
- Leveraging companies against each other
  - Example: Walmart lowers wages by 5%. As a result UnionCoin members from Walmart _and_ Target go on strike and protest, incentivizing Target to apply pressure to Walmart to raise wages
- Larger pool of [strike funds](https://en.wikipedia.org/wiki/Strike_pay)

The key to successfully operating this union is membership across branches and regions. Large companies can close a single branch, but don't have the resources to _find_ and fire 50% of their employees at 50% of their branches. The logistical nightmare combined with the loss in revenue will force acquiescence to demands.

## Operation

A cryptocurrency known as UnionCoin (uc) will be distributed using a daily (or sub-daily) dutch auction over a period of time. This will establish Ether liquidity for initial development and operation.

Union membership dues are paid as a yearly amount of UnionCoin based on the current market exchange rate, updated monthly.

A website for communication about union objectives will be built as a web application. Information about union operation will be open to the public; participation will be limited to those that have active memberships.

This website will allow users to indicate what company they work for and what governing entity they belong to. Members will be able to vote for objectives they would like to see completed; and contribute UnionCoin as objective funds for strikes and protests.

### Objectives

Objectives are changes the union would like to see occur at the company level. This could be wage increases, benefits changes, working condition improvements, additional hires, or anything that the community collectively stakes UnionCoin for.

Objectives can also exist at the government level. e.g. raise the federal minimum wage. Pressure should be applied to the government using strikes and protests against domestic companies, or government services.

Objectives should be written as long form, editable documents. A final document should be voted on by members staking UnionCoin for the objective. A minimum amount of UnionCoin should be defined for the objective to become active. If this minimum is not met all uc should be refunded.

Actions may be strikes or protests, described below.

- _Objectives should have clear success conditions_
- _Objectives should have clear termination conditions_
- _Objectives should have clearly defined terms_

#### Strikes

Strikes are organized worker walkouts. This can be in tandem with protests, or can simply be absence from work (in an effort to force the organization to involuntarily stop conducting business). Participation in a strike does not necessitate protest, especially in remote areas where protest may be infeasible or ineffective.

### Protests

Protests are organized gatherings of people (employees or not) at specific locations, optionally in conjunction with strikes.

Protests should be scheduled in advance with prospective attendees staking UnionCoin on attending the protest.

Attendees of the protest should be able to convene with the organizer at the location, receiving some or all of their staked UnionCoin as a refund.

Additionally organizers should be able to request funds from an objective to further compensate attendees (users that stake UnionCoin and then attend).

This would ideally be built into a small application, like a QR scanner on a smartphone to check in to the protest and receive the UnionCoin reward.

## Negotiation

Modern companies and governing entities have liaisons for social media such as Twitter, Facebook, Instagram, etc.

Accordingly companies should communicate on the UnionCoin website, with negotiations openly occurring on user defined objectives.

_All_ actions and negotiations should be openly visible on the UnionCoin website for all to see.

Transparency and fairness go hand in hand.

A company verification system should be established.

## Token Economics

As an ERC20 token UnionCoin will be tradeable on any cryptocurrency exchanges that choose to adopt it. Note that UnionCoin is not a security token, but instead a mechanic for organizing and defining membership in the union. As such the inherent value of the token is immaterial to the operation of the system.

The goal is to empower workers against companies that have previously had effectively uncontested power.

## Initial Distribution

100% of the total token supply should be initially distributed in a series of short dutch auctions for preset amounts of token.

### Distribution Mechanics

Every 6 hours a total of 10,000 tokens are auctioned starting at a high price and linearly decreasing in price until someone buys some, or all of the tokens up for auction.

Any unbought tokens at the end of the auction period become locked, reducing the total supply.

This will continue from the start of distribution for approximately 12 months, for a total of 34,560 cycles of 6 hours.

At the end of the final cycle all tokens will have been either distributed or destroyed. The total supply will not change (as a result of UnionCoin contract logic) at the end of distribution.

### Ether Raised

UnionCoin will be sold in exchange for ether during distribution. Collected ether will be used to fund platform development and operation.

## Communication

Open communication is key.

A discord server is available [here](https://discord.gg/meUVaHw).

All intellectual property should be openly licensed as [GPLv3](https://www.gnu.org/licenses/gpl-3.0.html).

### Security

Communication should be secured with ?PGP? signatures and encryption (when necessary).

## Community

<!-- Define X -->
The UnionCoin website allows anyone with a minimum of X tokens to participate and post on the platform. Due to this fact moderators will be necessary to prevent spam, disinformation, etc.

That said, content should not be arbitrarily removed, and removed content should be open for audit by anyone.
